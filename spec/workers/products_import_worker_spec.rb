require 'rails_helper'

describe ProductsImportWorker, type: :worker do
  describe '#perform' do
    let(:file_path) { File.join(Rails.root, 'spec/fixtures/products.csv') }

    it 'should import products via ProductsImporter' do
      expect_any_instance_of(ProductsImporter).to receive(:import)
      
      Sidekiq::Testing.inline! do
        described_class.perform_async(file_path)
      end
    end
  end
end

# frozen_string_literal: true

class FileSaver
  RELATIVE_SAVE_DIR_PATH = '/public/spree'

  def initialize(file)
    @file = file
  end

  def save
    ensure_save_dir
    full_file_path = File.join(Rails.root, RELATIVE_SAVE_DIR_PATH, new_file_name)
    File.write(full_file_path, @file.read)
    full_file_path
  end

  private

  def ensure_save_dir
    full_save_path = File.join(Rails.root, RELATIVE_SAVE_DIR_PATH)
    FileUtils.mkdir_p(full_save_path)
  end

  def new_file_name
    @new_file_name ||= "#{SecureRandom.uuid}#{File.extname(@file)}"
  end
end

# frozen_string_literal: true

class BackgroundJobStatus
  STATUS_MAPPING = {
    queued: :working,
    working: :working,
    complete: :complete,
    failed: :failed,
    interrupted: :failed
  }.freeze

  def initialize(job_id)
    @job_id = job_id
  end

  def status
    sidekiq_data = Sidekiq::Status.get_all(@job_id)
    sidekiq_status = sidekiq_data['status']
    errors = sidekiq_data['errors']
    { status: calc_job_status(sidekiq_status, errors.present?), message: errors }
  end

  private

  def calc_job_status(sidekiq_status, error_occured)
    error_occured ? STATUS_MAPPING[:failed] : STATUS_MAPPING[sidekiq_status.to_sym]
  end
end

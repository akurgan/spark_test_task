require 'rails_helper'

describe ProductImportsController, type: :controller do
  stub_authorization!

  describe '#create' do
    let(:params) { { file: fixture_file_upload('products.csv', 'text/csv') } }

    before do
      allow_any_instance_of(FileSaver).to receive(:save)
    end

    it 'should save file with FileSaver' do
      expect_any_instance_of(FileSaver).to receive(:save)

      post :create, params: params
    end  

    it 'should start ProductsImportWorker' do
      expect{ post :create, params: params }.to change(ProductsImportWorker.jobs, :size).by(1)
    end

    it 'should pass file path returned by FileSaver into ProductsImportWorker' do
      allow_any_instance_of(FileSaver).to receive(:save).and_return('/home/user/files/products.csv')
      expect(ProductsImportWorker).to receive(:perform_async).with('/home/user/files/products.csv')

      post :create, params: params
    end

    it 'should return JSON with ProductsImportWorker job id' do
      expect(ProductsImportWorker).to receive(:perform_async).and_return('3cb581e5fbfb006031db5821')

      post :create, params: params

      expect(JSON(response.body)).to eq({ 'job_id' => '3cb581e5fbfb006031db5821' })
    end
  end

  describe '#job_status' do
    it 'should return JSON returned by BackgroundJobStatus' do
      allow_any_instance_of(BackgroundJobStatus).to receive(:status).and_return({ status: :complete })

      get :job_status

      expect(JSON(response.body)).to eq({ 'status' => 'complete' })
    end
  end
end

require 'rails_helper'

describe 'Products Import' do
  context 'as admin' do
    stub_authorization!

    describe 'Products index page' do
      before { visit spree.admin_products_path }

      it 'should have import button' do
        expect(page).to have_content(I18n.t(:new_product_import))
      end

      describe 'import button' do
        it 'should redirect to new import page' do
          click_link :new_product_import
          expect(current_path).to eq main_app.new_product_import_path
        end
      end
    end

    describe 'Products import page', js: true do
      let(:products_csv) { 'spec/fixtures/products.csv' }

      before { visit main_app.new_product_import_path }

      it 'should have file input' do
        expect(page).to have_xpath '//input[@type="file"]'
      end

      it 'should have disabled import button' do
        expect(page).to have_button(I18n.t(:product_import_submit), disabled: true)
      end

      it 'should have cancel button link' do
        expect(page).to have_link Spree.t('actions.cancel'), href: spree.admin_products_path
      end

      context 'when an imports file has been chosen' do
        before { page.attach_file(:file, products_csv) }

        it 'should enable import button' do
          expect(page).to have_button(I18n.t(:product_import_submit), disabled: false)
        end
      end

      context 'when import button has been pressed' do
        before do
          page.attach_file(:file, products_csv)
          click_button(I18n.t(:product_import_submit))
        end

        it 'should start ProductsImportWorker' do
          expect(ProductsImportWorker.jobs.size).to eq 1
        end
       end

      # describe 'importing products' do
      #   before do
      #     visit main_app.new_product_import_path
      #     page.attach_file(:file, products_csv)
      #     click_button(I18n.t(:product_import_submit))
      #   end

      #   context 'with valid CSV' do
      #     let(:products_csv) { 'spec/fixtures/products.csv' }

      #     it 'should create products in database' do
      #       expect(Spree::Product.count).to eq 3
      #     end

      #     describe 'created product attributes' do
      #       let(:product) { Spree::Product.first }

      #       it { expect(product.name).to eq 'Ruby on Rails Bag' }
      #       it { expect(product.description).to eq 'Lorem Ipsum' }
      #       it { expect(product.price).to eq 22.99 }
      #       it { expect(product.available_on).to eq DateTime.parse('2017-12-04T14:55:22.913Z') }

      #       describe 'created product stock items' do
      #         let(:stock_item) { product.stock_items.first }

      #         it { expect(stock_item.count_on_hand).to eq 15 }
      #       end

      #       describe 'created product taxons' do
      #         let(:taxon) { product.taxons.first }

      #         it { expect(taxon.name).to eq 'Bags' }
      #       end
      #     end
      #   end

      #   context 'when CSV contains corrupted data' do
      #     let(:products_csv) { 'spec/fixtures/erroneous_products.csv' }

      #     it 'should display flash error message' do
      #       expect(page).to have_selector '.alert.alert-danger'
      #     end
      #   end
      # end
    end
  end
end

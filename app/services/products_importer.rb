# frozen_string_literal: true

class ProductsImporter
  def initialize(source:, parser: CsvProductsParser)
    @attributes = parser.new(source).parse
  end

  def import
    ensure_stock_location
    @attributes.each_with_index.map(&method(:create_product))
  end

  private

  def create_product(parsed_attributes, row_index)
    product_attributes = prepare_product_attributes(parsed_attributes)

    begin
      Spree::Product.transaction do
        product = Spree::Product.create!(product_attributes)
        update_stock_count(product, parsed_attributes[:stock_total])
      end
    rescue StandardError => e
      Rails.logger.error("Cannot import product from row #{row_index + 1}: #{e.message}")
      "Row #{row_index + 1}: #{e.message}"
    end
  end

  def prepare_product_attributes(parsed_attributes)
    product_attributes = parsed_attributes.except(:category, :stock_total)
    category = parsed_attributes[:category]
    product_attributes.merge(shipping_category: shipping_category, taxons: [fetch_taxon(category)])
  end

  def update_stock_count(product, stock_total)
    stock_item = product.stock_items.first
    return if stock_item.nil?

    unless stock_item.update(count_on_hand: stock_total)
      stock_item.errors.full_messages.first
    end
  end

  def ensure_stock_location
    Spree::StockLocation.first_or_create(name: 'Default')
  end

  def shipping_category
    @shipping_category ||= Spree::ShippingCategory.first_or_create(name: 'Default')
  end

  def categories_taxonomy
    @categories_taxonomy ||= Spree::Taxonomy.find_or_create_by(name: 'Categories')
  end

  def fetch_taxon(category)
    categories_taxonomy.taxons.find_or_create_by(name: category)
  end
end

require 'rails_helper'

describe BackgroundJobStatus do 
  let(:service) { described_class.new('job_id') }

  describe '#status' do
    shared_examples 'Sidekiq status mapping' do |sidekiq_status, returned_status|
      context 'when Sidekiq status is :queued' do
        before { allow(Sidekiq::Status).to receive(:status).and_return(sidekiq_status) }

        it 'should return hash with :working as status' do
          expect(service.status).to eq({ status: returned_status })
        end
      end
    end

    include_examples 'Sidekiq status mapping', :queued, :working
    include_examples 'Sidekiq status mapping', :working, :working
    include_examples 'Sidekiq status mapping', :complete, :complete
    include_examples 'Sidekiq status mapping', :failed, :failed
    include_examples 'Sidekiq status mapping', :interrupted, :failed
  end
end

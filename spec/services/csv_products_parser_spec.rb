require 'rails_helper'

describe CsvProductsParser, type: :service do 
  let(:products_csv) { File.open('spec/fixtures/products.csv') }
  let(:service) { described_class.new(products_csv) }

  describe '#parse' do
    it 'should return correct number of product attributes' do
      expect(service.parse.count).to eq 3
    end

    describe 'parsed attributes' do
      let(:product_attributes) { service.parse[0] }

      it { expect(product_attributes[:name]).to eq 'Ruby on Rails Bag' }
      it { expect(product_attributes[:description]).to eq 'Lorem Ipsum' }
      it { expect(product_attributes[:available_on]).to eq '2017-12-04T14:55:22.913Z' }
      it { expect(product_attributes[:price]).to eq '22.99' }
      it { expect(product_attributes[:category]).to eq 'Bags' }
      it { expect(product_attributes[:stock_total]).to eq '15' }
    end
  end
end

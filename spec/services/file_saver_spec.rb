require 'rails_helper'

describe FileSaver, type: :service do
  let(:save_dir) { '/public/test' }
  let(:save_dir_full_path) { File.join(Rails.root, save_dir) }
  let(:file) { File.open('spec/fixtures/products.csv') }
  let(:service) { described_class.new(file) }

  before { stub_const("#{described_class.name}::RELATIVE_SAVE_DIR_PATH", save_dir) }
  after { FileUtils.rm_rf(save_dir_full_path) }

  describe '#save' do
    let(:full_file_name) { File.join(save_dir_full_path, '191bb43f-7e61-495d-8fa6-d7a006522255.csv') }

    before do
      allow(SecureRandom).to receive(:uuid).and_return('191bb43f-7e61-495d-8fa6-d7a006522255')
      service.save
    end

    context 'when save file directory is not created' do
      it 'should create save file directory' do
        expect(Dir.exists?(save_dir_full_path)).to eq true
      end
    end

    it 'should save file in correct location with UUID name' do
      expect(File.exists?(full_file_name)).to eq true
    end

    it 'should return saved file absolute path' do
      expect(service.save).to eq full_file_name
    end

    it 'should copy file content into new file' do
      expected_content = File.read(full_file_name)
      file.rewind
      actual_content = file.read

      expect(actual_content).to eq expected_content
    end
  end
end

# frozen_string_literal: true

class ProductsImportWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options retry: false

  def perform(file_path)
    csv_file = File.new(file_path)
    store errors: ProductsImporter.new(source: csv_file).import
  end
end

require 'rails_helper'

describe ProductsImporter, type: :service do
  describe '#import' do
    let(:service) { described_class.new(source: 'mock_source') }

    let(:parsed_products) do
      JSON(File.read('spec/fixtures/parsed_products.json')).map(&:symbolize_keys)
    end

    before do
      allow_any_instance_of(CsvProductsParser).to receive(:parse).and_return(parsed_products)
    end

    shared_examples 'find or create required association' do |spec_params|
      context "when there is no default #{spec_params[:name]}" do
        it "should create default #{spec_params[:name]}" do
          expect { service.import }.to change(spec_params[:class], :count).from(0).to(1)
          expect(spec_params[:class].last.name).to eq spec_params[:default_name]
        end
      end

      context "when there is default #{spec_params[:name]}" do
        before { spec_params[:class].create!(name: spec_params[:default_name]) }

        it "should not create new #{spec_params[:name]}" do
          expect { service.import }.not_to change(spec_params[:class], :count)
        end
      end
    end

    include_examples 'find or create required association', 
                     class: Spree::StockLocation, name: 'stock location', default_name: 'Default'

    include_examples 'find or create required association', 
                     class: Spree::ShippingCategory, name: 'shipping category', default_name: 'Default'

    include_examples 'find or create required association', 
                     class: Spree::Taxonomy, name: 'Categories taxonomy', default_name: 'Categories' 

    it 'should create products in database' do
      expect { service.import }.to change(Spree::Product, :count).from(0).to(3)
    end

    describe 'created product attributes' do
      before { service.import }

      let(:product) { Spree::Product.first }

      it { expect(product.name).to eq 'Ruby on Rails Bag' }
      it { expect(product.description).to eq 'Lorem Ipsum' }
      it { expect(product.price).to eq 22.99 }
      it { expect(product.available_on).to eq DateTime.parse('2017-12-04T14:55:22.913Z') }

      describe 'created product stock items' do
        let(:stock_item) { product.stock_items.first }

        it { expect(stock_item.count_on_hand).to eq 15 }
      end

      describe 'created product taxons' do
        let(:taxon) { product.taxons.first }

        it { expect(taxon.name).to eq 'Bags' }
      end
    end

    context 'when products attrbiutes are invalid' do
      before do 
        allow(Spree::Product).to receive(:create!).and_raise(ActiveRecord::RecordInvalid)
      end

      it 'should return array of errors' do
        expect(service.import.size).to eq 3
      end
    end
  end
end

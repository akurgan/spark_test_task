# frozen_string_literal: true

require 'csv'

class CsvProductsParser
  def initialize(file)
    @csv = CSV.new(file, col_sep: ';', headers: true, skip_blanks: true)
  end

  def parse
    @csv
      .reject(&method(:empty_row?))
      .map(&method(:transform_row))
  end

  private

  def transform_row(row)
    {
      name:         row['name'],
      description:  row['description'],
      available_on: row['availability_date'],
      price:        parse_price(row),
      category:     row['category'],
      stock_total:  row['stock_total']
    }
  end

  def empty_row?(row)
    row.to_h.values.all?(&:blank?)
  end

  def parse_price(row)
    row['price']&.sub(',', '.')
  end
end


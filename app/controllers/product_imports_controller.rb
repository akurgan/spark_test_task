# frozen_string_literal: true

class ProductImportsController < Spree::Admin::BaseController
  def new; end

  def create
    file_path = FileSaver.new(params[:file].to_io).save
    job_id = ProductsImportWorker.perform_async(file_path)
    render json: { job_id: job_id }
  end

  def job_status
    render json: BackgroundJobStatus.new(params[:job_id]).status
  end
end
